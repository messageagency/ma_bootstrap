<?php

namespace Drupal\ma_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Plugin\Preprocess\Region as BootstrapRegion;
use Drupal\bootstrap\Utility\Variables;

/**
 * Pre-processes variables for the "region" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("region")
 */
class Region extends BootstrapRegion {

  /**
   * {@inheritdoc}
   */
  public function preprocessVariables(Variables $variables) {
    parent::preprocessVariables($variables);

    switch ($variables['elements']['#region']) {
      case 'top_bar':
        $variables['attributes']['role'] = 'banner';
        break;

      case 'admin_bar':
        $variables['attributes']['role'] = 'toolbar';
        break;

      case 'sidebar_first':
      case 'sidebar_second':
        $variables['region_wrapper'] = 'aside';
        $variables['col_classes'] = 'col-sm-3';
        $variables['attributes']['role'] = 'complementary';
        break;
    }
  }

}
