<?php

namespace Drupal\ma_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Utility\Variables;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;
use Drupal\ma_sprout_helpers\Helpers\SproutHelpers;
use Drupal\node\Entity\Node;
use Drupal\bootstrap\Plugin\Preprocess\Menu as BootstrapMenu;

/**
 * Pre-processes variables for the "menu" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("menu")
 */
class Menu extends BootstrapMenu implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  protected function preprocessVariables(Variables $variables) {
    parent::preprocessVariables($variables);
    $variables['attributes']['role'] = 'menu';

    if (isset($variables['menu_block_configuration'])) {
      if (isset($variables['menu_block_configuration']['suggestion'])) {
        switch ($variables['menu_block_configuration']['suggestion']) {
          // Subnav
          //case 'subnav':
          //break;
          // Gridnav
          case 'gridnav':
            foreach ($variables['items'] as $key => $menulink) {
              // Why is MenuLinkContent::getEntity protected?
              $params = $menulink['original_link']->getRouteParameters();
              if (!isset($params['node'])) {
                $variables['items'][$key]['img_info'] = [];
                continue;
              }
              $node = Node::load($params['node']);
              // TODO: It'd be better not to hardcode the field name and image style here.
              $img_info = SproutHelpers::getImageInfo($node, 'field_image', 'tile');
              $variables['items'][$key]['img_info'] = $img_info;
            }
            break;
        }
      }
    }
  }

}

