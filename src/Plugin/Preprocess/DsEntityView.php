<?php

namespace Drupal\ma_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Utility\Variables;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;

/**
 * Pre-processes variables for the "ds_entity_view" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("ds_entity_view")
 */
class DsEntityView extends PreprocessBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   * When display suite is controlling layout, use this hook instead of
   * preprocess_ENTITY() (node, paragraph, etc).
   *
   * @see template_preprocess_ds_entity_view() in ds.module.
   */
  public function preprocessVariables(Variables $variables) {
    $content = $variables['content'];

    // Paragraph entities.
    if (isset($variables['content']['#paragraph'])) {
      $variables['content']['#attributes']['id'] = 'paragraph-' . $variables['content']['#paragraph']->id();
      switch ($content['#bundle']) {
        case 'billboard':
          // Add 'no-img' class if there is no background image.
          if (isset($content['field_pg_image']) && !isset($content['field_pg_image'][0])) {
            $variables['content']['#settings']['layout']['classes']['no-img'] = 'no-img';
          }
          break;
      }
    }
  }

}

