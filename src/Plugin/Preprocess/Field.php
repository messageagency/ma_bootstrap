<?php

namespace Drupal\ma_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Utility\Variables;
use Drupal\bootstrap\Utility\Element;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;
use Drupal\ma_sprout_helpers\Helpers\SproutHelpers;

/**
 * Pre-processes variables for the "field" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("field")
 */
class Field extends PreprocessBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  protected function preprocessElement(Element $element, Variables $variables) {

    switch ($variables['field_type']) {
      // If a datetime fieldname ends with '_start', look for a corresponding
      // field ending in '_end' and pass it as a renderable array to a template.
      // This is an ugly work around for lack of a stable daterange field type.
      case 'datetime':
        $i = strpos($variables['field_name'], '_start');
        if ($i === FALSE) {
          break;
        }
        $start_date_timeless_field = substr($variables['field_name'], 0, $i) . '_start_timeless';
        foreach ($variables['items'] as $key => $item) {
          if (is_int($key)) {
            $inner_key = '#text';
            if (isset($variables['items'][$key]['content']['#markup'])) {
              $inner_key = '#markup';
            }
            $variables['items'][$key]['content'][$inner_key] = $this->processTimeless($element['#object'], $start_date_timeless_field, $element[$key][$inner_key]);
          }
        }
        $end_date_field = substr($variables['field_name'], 0, $i) . '_end';
        $end_date_items = [];
        if ($element['#object']->hasField($end_date_field) == FALSE) {
          break;
        }
        // The end date must be enabled on the same view mode as the start date
        // (it can be in the hidden region) and should be using the same format.
        $end_date = $element['#object']->$end_date_field->view($element['#view_mode']);
        if (!isset($end_date['#field_type'])
          || $end_date['#field_type'] !== 'datetime'
          || !isset($end_date['#view_mode'])
          || $end_date['#view_mode'] !== $element['#view_mode']) {
          break;
        }
        foreach ($end_date as $key => $item) {
          if (is_int($key)) {
            //TODO: find a better way to separate date and time.
            $start = explode(' - ', $element[$key][$inner_key]);
            $end = explode(' - ', $item[$inner_key]);
            $end_date_timeless_field = substr($variables['field_name'], 0, $i) . '_end_timeless';
            if ($start[0] == $end[0]) {
              if (!isset($end[1])) {
                break;
              }
              $end_text = $this->processTimeless($element['#object'], $end_date_timeless_field, $item[$inner_key]);
              if ($end_text == $end[0]) {
                break;
              }
              else {
                $item[$inner_key] = $end[1];
              }
            }
            else {
              $item[$inner_key] = $this->processTimeless($element['#object'], $end_date_timeless_field, $item[$inner_key]);
            }
            $end_date_items[$key]['content'] = $item;
          }
        }
        $variables['end_date_items'] = $end_date_items;
        break;
    }

    switch ($variables['field_name']) {
      // subnav menu block fields.
      case 'dynamic_block_field:node-subnav':
      case 'dynamic_block_field:node-subnav_secondary':
      case 'dynamic_block_field:node-subnav_supplemental':
        $variables['title_wrapper'] = 'h3';
        // change field label to menu "root" label, to match the block instance.
        // @see blockLabel() in menu_block/src/Plugin/Block/menuBlock.php
        if ($element[0][0]['#menu_block_configuration']['label_type'] == 'root') {
          $pluginId = key($element[0][0]['#items']);
          $block_label = SproutHelpers::getMenuLinkParentLabel($pluginId);
          $variables['label'] = $block_label;
        }
        // accordion behavior.
        $variables['title_attributes']['data-toggle'] = 'collapse';
        $variables['title_attributes']['data-target'] = '#accordion-subnav';
        $variables['items'][0]['attributes']['id'] = 'accordion-subnav';
        break;

      // Paragraph Related content, Signposts fields.
      case 'field_pg_content':
      case 'field_pg_signposts':
        $variables['item_classes'] = [
          'col-xs-12',
          'col-sm-6',
          'col-md-4',
          'field--item',
        ];
        $variables['attributes']['class'][] = 'row';
        break;

      // Paragraph Heading field.
      case 'field_pg_heading':
        $variables['field_wrapper'] = 'h2';
        if ($element['#bundle'] == 'billboard') {
          $variables['field_wrapper'] = 'h1';
          $variables['attributes']['class'][] = 'page-title';
        }
        break;

      // Paragraph Call to action field.
      case 'field_pg_cta':
        // We want the classes on the <a> tag, which does not accept an Attribute
        // object the way the field itself does.
        foreach ($variables['items'] as $delta => $item) {
          $btn_style = 'btn-default';
          if ($delta > 0) {
            $variables['field_wrapper'] = 'span';
            $btn_style = 'btn-primary';
          }
          $variables['items'][$delta]['content']['#options']['attributes'] = [
            'class' => [
              'btn',
              $btn_style,
              'btn-lg',
            ],
          ];
        }
        break;

      // Paragraph Signpost heading field.
      case 'field_signpost_heading':
        $variables['field_wrapper'] = 'h3';
        break;
    }
  }

  /**
   * Helper function to remove times if field is timeless.
   *
   * @param mixed $object
   *   The object to reference.
   * @param string $field_name
   *   Name of the all day field to check against
   * @param string $text
   *   Text to modify and return.
   *
   * @return string $text
   *   Modfied text
   */
  protected function processTimeless($object, $field_name, $text) {
    if ($object->hasField($field_name) == FALSE) {
      return $text;
    }
    else {
      $timeless_value = $object->{$field_name}->getValue();
      if (!empty($timeless_value) && $timeless_value[0]['value'] == '1') {
        $text_array = explode(' - ', $text);
        return $text_array[0];
      }
      else {
        return $text;
      }
    }

  }
}

