<?php

namespace Drupal\ma_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Utility\Variables;
use Drupal\bootstrap\Utility\Element;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;

/**
 * Pre-processes variables for the "paragraph" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("paragraph")
 */
class Paragraph extends PreprocessBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  protected function preprocessElement(Element $element, Variables $variables) {
    // This only applies when display suite is not used.
    $variables['attributes']['id'] = 'paragraph-' . $variables['paragraph']->id();
  }

}

