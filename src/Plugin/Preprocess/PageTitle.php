<?php

namespace Drupal\ma_bootstrap\Plugin\Preprocess;

use Drupal\bootstrap\Utility\Variables;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;
use Drupal\node\Entity\Node;

/**
 * Pre-processes variables for the "page_title" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("page_title")
 */
class PageTitle extends PreprocessBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocessVariables(Variables $variables) {
    $variables['rendered_by_ds'] = FALSE;
    // Get the node or null from the route.
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node && $node instanceof Node) {
      // When loading revisions of a code, the above code returns a string instead of an object.
      // See https://www.drupal.org/node/2730631
      // If we didn't get back an object, load the node object explicitly.
      if (is_numeric($node)) {
        $node = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->load($node);
        if (!$node) {
          return;
        }
      }
      // Get the full content display of the node, or null.
      $display = \Drupal::entityTypeManager()
        ->getStorage('entity_view_display')
        ->load('node' . '.' . $node->getType() . '.' . 'full');
      if ($display) {
        // Get ds settings from display, or empty array.
        $third_party = $display->getThirdPartySettings('ds');
        if ($third_party && isset($third_party['fields'])) {
          if (isset($third_party['fields']['node_title'])) {
            $variables['rendered_by_ds'] = TRUE;
          }
        }
      }
    }
  }

}

